// SPDX-License-Identifier: MIT
import "hardhat/console.sol";

pragma solidity ^0.8.0;

interface ERC20 {
    function totalSupply() external view returns (uint256);

    function balanceOf(address tokenOwner) external view returns (uint256);

    function allowance(address tokenOwner, address spender)
        external
        view
        returns (uint256);

    function transfer(address to, uint256 tokens) external returns (bool);

    function approve(address spender, uint256 tokens) external returns (bool);

    function transferFrom(
        address from,
        address to,
        uint256 tokens
    ) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint256 tokens);
    event Approval(
        address indexed tokenOwner,
        address indexed spender,
        uint256 tokens
    );
}

contract Trololoken is ERC20 {
    string public name;
    string public symbol;
    uint256 public _totalSupply;
    address public owner;

    mapping(address => uint256) balances;
    mapping(address => mapping(address => uint256)) allowed;

    constructor() {
        name = "Trololoken";
        symbol = "TROL";
        _totalSupply = 10000;
        owner = msg.sender;

        balances[msg.sender] = _totalSupply;
        emit Transfer(address(0), msg.sender, _totalSupply);
    }

    modifier restricted() {
        require(msg.sender == owner, "Must be the owner");
        _;
    }

    function totalSupply() public view override returns (uint256) {
        return _totalSupply - balances[address(0)];
    }

    function balanceOf(address addr) public view override returns (uint256) {
        return balances[addr];
    }

    function allowance(address _owner, address _spender) 
        public
        view
        override
        returns (uint256)
    {
        return allowed[_owner][_spender];
    }

    function approve(address _spender, uint256 value)
        public
        override
        returns (bool)
    {
        allowed[msg.sender][_spender] = value;
        emit Approval(msg.sender, _spender, value);
        return true;
    }

    function transfer(address to, uint256 value) public override returns (bool) {
        balances[msg.sender] -= value;
        balances[to] += value;
        emit Transfer(msg.sender, to, value);
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) public override returns (bool) {
        require(balances[from] >= value, "Not enought ether");
        require(allowed[from][to] >= value, "Not allowed to transfer this much");

        unchecked {
            balances[from] -= value;
            balances[to] += value;
            allowed[from][to] -= value;
        }
        return true;
    }

    function mint(address account, uint256 value) public restricted {
        _totalSupply += value;
        balances[account] += value;
        emit Transfer(address(0), account, value);
    }

    function burn(address account, uint256 value) public restricted {
        require(
            balances[account] >= value,
            "burn amount exceeds balance"
        );
        balances[account] -= value;
        _totalSupply -= value;
        emit Transfer(account, address(0), value);
    }
}
