import { expect } from 'chai'
import { ethers } from 'hardhat'
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers'
import { Trololoken } from '../typechain'

let trololoken: Trololoken
let owner: SignerWithAddress
let addr1: SignerWithAddress
let addr2: SignerWithAddress

beforeEach(async function () {
  ;[owner, addr1, addr2] = await ethers.getSigners()

  const Trololoken = await ethers.getContractFactory('Trololoken')
  trololoken = await Trololoken.deploy()
  await trololoken.deployed()
})

describe('Trololoken', function () {
  it('Should set the right owner', async () => {
    expect(await trololoken.owner()).to.equal(owner.address)
  })

  it('Should get totalSupply', async function () {
    expect(await trololoken.totalSupply()).to.equal(10000)
  })

  it('Should check balance', async function () {
    expect(await trololoken.balanceOf(owner.address)).to.equal(10000)
  })

  it('Should transfer properly', async function () {
    await trololoken.transfer(addr1.address, 100)
    expect(await trololoken.balanceOf(addr1.address)).to.equal(100)
  })

  it('Should approve', async function () {
    await trololoken.approve(addr1.address, 100)
    expect(
      await trololoken.allowance(owner.address, addr1.address)
    ).to.be.equal(100)
  })

  it('Should transfer when allowed', async function () {
    await trololoken.transfer(addr1.address, 1000)
    await trololoken.connect(addr1).approve(addr2.address, 110)
    await trololoken.transferFrom(addr1.address, addr2.address, 100)
    expect(await trololoken.balanceOf(addr2.address)).to.be.equal(100)
  })

  it('Should fail to transfer if not enough ether', async function () {
    await expect(
      trololoken.connect(addr1).transferFrom(addr1.address, addr2.address, 100)
    ).to.be.revertedWith('Not enought ether')
  })

  it('Should fail to transfer if not enough ether', async function () {
    await trololoken.transfer(addr1.address, 1000)
    await trololoken.connect(addr1).approve(addr2.address, 50)
    await expect(
      trololoken.transferFrom(addr1.address, addr2.address, 100)
    ).to.be.revertedWith('Not allowed to transfer this much')
  })

  it('Shoult emit event Transfer', async function () {
    await expect(trololoken.transfer(addr1.address, 100))
      .to.emit(trololoken, 'Transfer')
      .withArgs(owner.address, addr1.address, 100)
  })

  it('Shoult emit event Approval', async function () {
    await expect(trololoken.approve(addr1.address, 100))
      .to.emit(trololoken, 'Approval')
      .withArgs(owner.address, addr1.address, 100)
  })

  it('Shoult mint', async function () {
    await trololoken.mint(owner.address, 100)
    expect(await trololoken.totalSupply()).to.be.equal(10100)
  })

  it('Should fail mint if not owner', async () => {
    await expect(
      trololoken.connect(addr1).mint(owner.address, 100)
    ).to.be.revertedWith('Must be the owner')
  })

  it('Shoult burn', async function () {
    await trololoken.burn(owner.address, 100)
    expect(await trololoken.totalSupply()).to.be.equal(9900)
  })

  it('Shoult fail burn if balance not enough', async function () {
    await expect(trololoken.burn(owner.address, 999999)).to.be.revertedWith(
      'burn amount exceeds balance'
    )
  })
})
