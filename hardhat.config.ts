import * as dotenv from 'dotenv'

import { HardhatUserConfig, task } from 'hardhat/config'
import '@nomiclabs/hardhat-etherscan'
import '@nomiclabs/hardhat-waffle'
import '@typechain/hardhat'
import 'hardhat-gas-reporter'
import 'solidity-coverage'

dotenv.config()

const { ROPSTEN_URL, PRIVATE_KEY, INFURA_URL, REPORT_GAS, ETHERSCAN_API_KEY } =
  process.env

task('transfer', 'Transfer tokens')
  .addParam('address', "The contract's address")
  .addParam('to', 'address of recipient')
  .addParam('value', 'value to send')
  .setAction(async (taskArgs, hre) => {
    const { address, to, value } = taskArgs
    const Trololoken = await hre.ethers.getContractFactory('Trololoken')
    const trololoken = await Trololoken.attach(address)
    const transfer = await trololoken.transfer(to, value)
    console.log(transfer)
  })

task('transferFrom', 'Transfer tokens between accounts')
  .addParam('address', "The contract's address")
  .addParam('from', 'address of sender')
  .addParam('to', 'address of recipient')
  .addParam('value', 'value to send')
  .setAction(async (taskArgs, hre) => {
    const { address, from, to, value } = taskArgs
    const Trololoken = await hre.ethers.getContractFactory('Trololoken')
    const trololoken = await Trololoken.attach(address)
    const transferFrom = await trololoken.transferFrom(from, to, value)
    console.log(transferFrom)
  })

task('approve', 'Approve to send tokens')
  .addParam('address', "The contract's address")
  .addParam('spender', 'address of spender')
  .addParam('value', 'value to allow')
  .setAction(async (taskArgs, hre) => {
    const { address, spender, value } = taskArgs
    const Trololoken = await hre.ethers.getContractFactory('Trololoken')
    const trololoken = await Trololoken.attach(address)
    const approve = await trololoken.approve(spender, value)
    console.log(approve)
  })

const config: HardhatUserConfig = {
  solidity: '0.8.4',
  defaultNetwork: 'hardhat',
  networks: {
    ropsten: {
      url: ROPSTEN_URL || '',
      accounts: PRIVATE_KEY !== undefined ? [PRIVATE_KEY] : []
    },
    rinkeby: {
      url: INFURA_URL,
      accounts: [`0x${PRIVATE_KEY}`]
    }
  },
  gasReporter: {
    enabled: REPORT_GAS !== undefined,
    currency: 'USD'
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY
  }
}

export default config
