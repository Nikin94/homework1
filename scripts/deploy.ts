const { ethers } = require('hardhat')

async function main() {
  const [deployer] = await ethers.getSigners()
  console.log(`Deploying contracts with the account: ${deployer.address}`)

  const balance = await deployer.getBalance()
  console.log(`Account balance: ${balance.toString()}`)

  const Trololoken = await ethers.getContractFactory('Trololoken')
  const trololoken = await Trololoken.deploy()
  console.log(`Contract address: ${trololoken.address}`)
}

main().catch(error => {
  console.error(error)
  process.exitCode = 1
})
